from constraint import *


# Ege ALPAY 19551

# Block starts from edge/black box and ends with edge/black box.
# It can be vertical or horizontal
def find_horizontal_vertical_blocks(rows, columns, black_boxes):
    blocks = []

    # Horizontal blocks
    for i in range(0, rows):
        temp = []
        for j in range(0, columns):
            # Block continuous
            if (i, j) not in black_boxes:
                temp.append((i, j))
            # Block ends
            if ((i, j) in black_boxes and len(temp) > 0) or (j == columns - 1):
                blocks.append(temp)
                temp = []

    # Vertical blocks
    for j in range(0, columns):
        temp = []
        for i in range(0, rows):
            # Block continuous
            if (i, j) not in black_boxes:
                temp.append((i, j))
            # Block ends
            if ((i, j) in black_boxes and len(temp) > 0) or (i == rows - 1):
                blocks.append(temp)
                temp = []

    return blocks


# Method to find adjacent white boxes of black box with hint
def find_available_adjacent_boxes(rows, columns, box, black_boxes):
    available_boxes = []
    i = box[0]  # Current Row
    j = box[1]  # Current Column
    # check left
    if j > 0:
        # If this one is white box
        if (i, j - 1) not in black_boxes:
            available_boxes.append((i, j - 1))
    # check right
    if j < columns - 1:
        # If this one is white box
        if (i, j + 1) not in black_boxes:
            available_boxes.append((i, j + 1))
    # check up
    if i > 0:
        # If this one is white box
        if (i - 1, j) not in black_boxes:
            available_boxes.append((i - 1, j))
    # check down
    if i < rows - 1:
        # If this one is white box
        if (i + 1, j) not in black_boxes:
            available_boxes.append((i + 1, j))

    hint = puzzle[i][j]

    return hint, available_boxes


# Method to find all black boxes
def find_black_boxes(rows, columns):
    black_boxes = []  # All black boxes
    black_boxes_hint = []  # Black boxes with hint(number)
    for i in range(0, rows):
        for j in range(0, columns):
            # If this box is black
            if puzzle[i][j] == "X":
                # Add it to list of all black boxes
                black_boxes.append((i, j))
            # If box has hint
            if puzzle[i][j] == 0 or puzzle[i][j] == 1 or puzzle[i][j] == 2 or puzzle[i][j] == 3 or puzzle[i][j] == 4:
                # Add it to list of all black boxes AND list of boxes with hint
                black_boxes_hint.append((i, j))
                black_boxes.append((i, j))

    return black_boxes, black_boxes_hint


def solve_akari():
    # Initializing problem
    akari = Problem()

    # Size of the board
    rows = len(puzzle)
    columns = len(puzzle[0])

    # White boxes as Variables
    for i in range(0, rows):
        for j in range(0, columns):
            if puzzle[i][j] == "()":
                akari.addVariable((i, j), range(0, 2))

    black_boxes, black_boxes_hint = find_black_boxes(rows, columns)

    # Check white boxes around black boxes with hint
    for box in black_boxes_hint:
        hint, available_boxes = find_available_adjacent_boxes(rows, columns, box, black_boxes)

        # Number of lights in available_boxes will be equal to hint
        akari.addConstraint(ExactSumConstraint(hint), available_boxes)

    blocks = find_horizontal_vertical_blocks(rows, columns, black_boxes)
    for item in blocks:
        # There can be at most 1 bulb in a block
        akari.addConstraint(MaxSumConstraint(1), item)

    # To finding all other white boxes in row and column of the current white box
    for current_row in range(0, rows):
        for current_col in range(0, columns):
            # If current box is white
            if puzzle[current_row][current_col] == "()":
                # Add current white box to list
                block_list = [(current_row, current_col)]
                temp_row = current_row
                temp_col = current_col

                # Check Left Row
                while temp_col > 0 and puzzle[temp_row][temp_col - 1] == "()":
                    temp_col -= 1
                    # If left box is white, add it to list
                    block_list.append((temp_row, temp_col))

                temp_col = current_col
                # Check Right Row
                while temp_col < columns - 1 and puzzle[temp_row][temp_col + 1] == "()":
                    temp_col += 1
                    # If right box is white, add it to list
                    block_list.append((temp_row, temp_col))

                temp_col = current_col
                # Check Upper Column
                while temp_row > 0 and puzzle[temp_row - 1][temp_col] == "()":
                    temp_row -= 1
                    # If upper box is white, add it to list
                    block_list.append((temp_row, temp_col))

                temp_row = current_row
                # Check Lower Column
                while temp_row < rows - 1 and puzzle[temp_row + 1][temp_col] == "()":
                    temp_row += 1
                    # If lower box is white, add it to list
                    block_list.append((temp_row, temp_col))

                # There should be at least 1 bulb in the direction of row and column of the current white box
                akari.addConstraint(MinSumConstraint(1), block_list)

    solutions = akari.getSolutions()
    solutions = solutions[0]

    coordinates_have_bulb = []

    for coordinate in solutions:
        # Only choose the boxes with bulb
        if solutions[coordinate] == 1:
            coordinates_have_bulb.append(coordinate)

    return coordinates_have_bulb


puzzle_easy = [
    ["X", "()", "()", "X", "()", "()", "X"],
    ["()", "X", "()", "()", "()", 3, "()"],
    ["()", "()", "()", "()", "()", "()", "()"],
    [0, "()", "()", "()", "()", "()", "X"],
    ["()", "()", "()", "()", "()", "()", "()"],
    ["()", 1, "()", "()", "()", 2, "()"],
    ["X", "()", "()", 1, "()", "()", 1]
]

puzzle_normal = [
    ["X", "()", "()", "()", "()", "()", 1],
    ["()", "()", 1, "()", "()", "()", "()"],
    ["()", "()", "()", 3, "()", "X", "()"],
    ["()", "()", 4, "()", 3, "()", "()"],
    ["()", 3, "()", "X", "()", "()", "()"],
    ["()", "()", "()", "()", 1, "()", "()"],
    ["X", "()", "()", "()", "()", "()", "X"]
]

puzzle_hard = [
    ["()", "()", "()", 1, 1, "()", "()"],
    ["()", "X", "()", "()", "()", "X", "()"],
    ["X", "()", "()", "()", "()", "()", "()"],
    ["X", "()", "()", "()", "()", "()", 1],
    ["()", "()", "()", "()", "()", "()", "X"],
    ["()", 3, "()", "()", "()", 0, "()"],
    ["()", "()", 1, 0, "()", "()", "()"]
]

puzzle_8x8 = [
    ["()", "()", 2, "()", "()", 2, "()", "()"],
    ["()", 3, "()", "()", "()", "()", "()", "()"],
    ["()", "()", "X", "X", "()", 3, "()", 0],
    ["()", "()", "()", "()", "()", "()", "X", "()"],
    ["()", "X", "()", "()", "()", "()", "()", "()"],
    [2, "()", 3, "()", "X", "X", "()", "()"],
    ["()", "()", "()", "()", "()", "()", 3, "()"],
    ["()", "()", "X", "()", "()", 2, "()", "()"]
]

puzzle = puzzle_hard
boxes_with_bulb = solve_akari()

print(boxes_with_bulb)
